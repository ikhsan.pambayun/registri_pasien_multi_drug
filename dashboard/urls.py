from django.urls import path
from . import views

app_name = 'dashboard'

urlpatterns = [

    path('', views.dashboard_view, name='index'),

    path('data/jenis-kelamin/', views.get_data_jenis_kelamin,
         name='data-jenis-kelamin'),
    path('data/alamat/', views.get_data_tahun_kasus, name='data-alamat'),
    path('data/output/', views.get_data_output, name='data-output'),
]
