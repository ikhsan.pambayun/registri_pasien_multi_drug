from django.shortcuts import render

from django.http import JsonResponse
from django.db.models import Count

import datetime
import math
import random

from pasien.models import Pasien
from django.contrib.auth.decorators import login_required

# Dashboard


@login_required(login_url='pasien:login')
def dashboard_view(request):
    return render(request, 'dashboard.html', {})


def dynamic_color():
    red = math.floor(random.randint(0, 255))
    green = math.floor(random.randint(0, 255))
    blue = math.floor(random.randint(0, 255))
    return "rgb(%d,%d,%d)" % (red, green, blue)


def get_data_jenis_kelamin(x):
    jenis_kelamin_count = Pasien.objects.all().values("jenis_kelamin").annotate(
        count=Count("jenis_kelamin")).order_by("jenis_kelamin")
    return JsonResponse({
        "type": "doughnut",
        "responsive": True,
        "maintainAspectRatio": False,
        "responsiveAnimationDuration": 250,
        "data": {
            "labels": [
                dict(Pasien.JENIS_KELAMIN_CHOICES).get(x["jenis_kelamin"],
                                                       x["jenis_kelamin"])
                for x in jenis_kelamin_count if x["jenis_kelamin"]
            ],
            "datasets": [{
                "borderColor":
                "#1f8ef1",
                "backgroundColor":
                [dynamic_color() for _ in range(len(jenis_kelamin_count))],
                "borderWidth":
                2,
                "data":
                [x["count"] for x in jenis_kelamin_count if x["jenis_kelamin"]],
            }]
        }
    })


def get_data_tahun_kasus(_):
    kasus_count = Pasien.objects.all().values("tahun_kasus").annotate(
        count=Count("tahun_kasus")).order_by("tahun_kasus")
    return JsonResponse({
        "type": "pie",
        "responsive": True,
        "maintainAspectRatio": False,
        "responsiveAnimationDuration": 250,
        "data": {
            "labels": [x["tahun_kasus"] for x in kasus_count if x["tahun_kasus"]],
            "datasets": [{
                "borderColor":
                "#1f8ef1",
                "backgroundColor":
                [dynamic_color() for _ in range(len(kasus_count))],
                "borderWidth":
                2,
                "data": [x["count"] for x in kasus_count if x["tahun_kasus"]],
            }]
        }
    })


def get_data_output(_):
    output_count = Pasien.objects.all().values("output").annotate(
        count=Count("output")).order_by("output")
    return JsonResponse({
        "type": "pie",
        "responsive": True,
        "maintainAspectRatio": False,
        "responsiveAnimationDuration": 250,
        "data": {
            "labels": [x["output"] for x in output_count if x["output"]],
            "datasets": [{
                "borderColor":
                "#1f8ef1",
                "backgroundColor":
                [dynamic_color() for _ in range(len(output_count))],
                "borderWidth":
                2,
                "data": [x["count"] for x in output_count if x["output"]],
            }]
        }
    })
