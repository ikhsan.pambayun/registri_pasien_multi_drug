from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponseNotAllowed, HttpResponse
from django.urls import reverse_lazy
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.decorators import login_required

from .forms import PasienForm, ExcelUploadForm
from .models import Pasien

import datetime
from openpyxl import load_workbook, Workbook
from openpyxl.utils import get_column_letter
from openpyxl.writer.excel import save_virtual_workbook
from openpyxl.styles import Border, Side, Alignment, PatternFill
import copy
# Pasien

def loginAdmin(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            return redirect('dashboard:index')
        
        else:
            messages.info(request, 'Username atau Password salah')
        
        return redirect('pasien:login')

    return render(request, 'login.html')

@login_required(login_url='pasien:login')
def logoutAdmin(request):
    logout(request)

    return redirect('pasien:login')

@login_required(login_url='pasien:login')
def list_view(request):
    if request.method == "GET":
        pasien = Pasien.objects.all()
        header = Pasien.get_header()

        context = {'pasien': pasien, 'header': header,
                   "excel_upload_form": ExcelUploadForm()}
        return render(request, 'list-pasien.html', context)

    if request.method == "POST":
        context = {"excel_upload_form": ExcelUploadForm()}
        return post_import_excel_page(request, context)

    return HttpResponseNotAllowed(["GET", "POST"])

@login_required(login_url='pasien:login')
def forms(request):
    pasien_form = PasienForm(request.POST or None)
    if request.method == 'POST':
        if pasien_form.is_valid():
            pasien_form.save()

        return redirect('pasien:index')

    context = {
        'title': 'Formulir Pasien',
        'pasien_form': pasien_form
    }

    return render(request, 'forms-pasien.html', context)

@login_required(login_url='pasien:login')
def delete_pasien(request, nomor):
    if request.method == 'POST':
        pasien = get_object_or_404(Pasien, no=nomor)
        pasien.delete()

        return redirect('pasien:index')
    return HttpResponseNotAllowed(["POST"])

def edit_pasien(request, nomor):
    instance = Pasien.objects.get(no=nomor)
    data = instance.dict_value()
    pasien_form = PasienForm(request.POST or None, initial=data, instance=instance)

    context = {"pasien_form": pasien_form}

    if request.method == "POST":
        if pasien_form.is_valid():
            pasien_form.save()

        return redirect('pasien:index')

    if request.method == "GET":
        return render(request, 'forms-pasien.html', context)
        
    return HttpResponseNotAllowed(["GET", "POST"])

def import_file(request):
    response = {"excel_upload_form": ExcelUploadForm()}
    if request.method == "GET":
        return get_import_excel_page(request, response)
    if request.method == "POST":
        return post_import_excel_page(request, response)
    return HttpResponseNotAllowed(["GET", "POST"])

def get_import_excel_page(request, response):
    return render(request, "import.html", response)

def post_import_excel_page(request, response):
    Pasien.objects.all().delete()
    form = ExcelUploadForm(request.POST, request.FILES)
    if form.is_valid():
        workbook = load_workbook(request.FILES["excel_file"])
        sheet = workbook.active

        for row in sheet.iter_rows(min_row=3, values_only=True, max_col=67):
            row = list(row)
            row[4] = check(row[4])
            row[8] = check(row[8])
            row[9] = check(row[9])
            row[39] = check(row[39])

            for i in range(67):
                if row[i] == '' or row[i] == ' ' :
                    row[i] = None

            pasien = Pasien(*row)

            pasien.save()

    return redirect(reverse_lazy('pasien:index'))

def check(x):
    if (not isinstance(x, datetime.date)):
        return None
    return x


def export_to_excel():
    wb = Workbook()

    ws1 = wb.active
    ws1.title = "Pasien"

    list_title1 = ["No", "Nama", "No MedRek", "No HP", "Tgl Lahir", "Jenis Kelamin", "Tahun Kasus", "Alamat", "Tanggal MRS", "Tanggal mulai OAT", "Jumlah pengobatan TBC sebelumnya", "Jenis Resistensi", "Jenis TBC", ]
    list_title2 = []

    for i in range (0, 8):
        if (i == 0):
            list_title1.append("Anamnesis")
        else:
            list_title1.append("blank")

    for i in range(0,6):
        if(i == 0):
            list_title1.append("Pemeriksaan fisik")
        else:
            list_title1.append("blank")

    for i in range(0,9):
        if(i == 0):
            list_title1.append("Penunjang")
        else:
            list_title1.append("blank")

    list_title1 = list_title1 + ["Terapi"]

    for i in range(0,4):
        if(i == 0):
            list_title1.append("Sumber kontak dewasa")
        else:
            list_title1.append("blank")

    list_title1 = list_title1 + ["Tempat ambil obat"]

    for i in range(0,24):
        if(i == 0):
            list_title1.append("Follow up")
        else:
            list_title1.append("blank")

    list_title1 = list_title1 + ["Output"]

    # Append head to file
    ws1.append(list_title1)

    # head2 file
    for i in range(0,13):
        list_title2.append("blank")
    list_title2 = list_title2 + ["Batuk", "Dahak", "Demam", "Sesak", "Keringat Malam", "Penurunan BB", "Hambatan Pertumbuhan", "Penurunan Nafsu Makan"]
    list_title2 = list_title2 + ["BB", "TB", "RR", "Suhu", "KGB", "Pulmo"]
    list_title2 = list_title2 + ["BTA 1", "BTA 2", "BTA 3", "Gene Xpert", "Kultur MTB", "Drug", "Foto Toraks", "Tes PPD", "LPA"]
    list_title2 = list_title2 + ["blank", "Nama", "Hubungan", "Tgl lahir", "Jenis kasus TB"]
    list_title2 = list_title2 + ["blank"]

    # untuk follow up
    for i in range(0,24):
        list_title2 = list_title2 + [i + 1]
    ws1.append(list_title2)


    # masukin data
    all_pasien = Pasien.objects.all().order_by("no")
    for pasien in all_pasien:
        ws1.append(pasien.get_all_value())


    # dimension size
    for col in range(2, 8):
        ws1.column_dimensions[get_column_letter(col)].width = 25

    # size Alamat
    ws1.column_dimensions[get_column_letter(8)].width = 60

    # size Tanggal MRS - Jenis TBC
    ws1.column_dimensions[get_column_letter(9)].width = 20
    ws1.column_dimensions[get_column_letter(10)].width = 20
    ws1.column_dimensions[get_column_letter(11)].width = 35
    ws1.column_dimensions[get_column_letter(12)].width = 20
    ws1.column_dimensions[get_column_letter(13)].width = 20

    # col size Anamnesis (Batuk - Penurunan)
    for col in range(14, 22):
        ws1.column_dimensions[get_column_letter(col)].width = 20

    # col size Pemeriksaan fisik (BB - Pulmo)
    for col in range(22, 26):
        ws1.column_dimensions[get_column_letter(col)].width = 20
    for col in range(26, 28):
        ws1.column_dimensions[get_column_letter(col)].width = 40

    # col size Penunjang (BTA1 - LPA)
    for col in range(28, 31):
        ws1.column_dimensions[get_column_letter(col)].width = 20
    for col in range(31, 37):
        ws1.column_dimensions[get_column_letter(col)].width = 35
        
    # col size Terapi
    ws1.column_dimensions[get_column_letter(37)].width = 60

    # col size Sumber kontak dewasa + Tempat ambil obat
    for col in range(38, 43):
        ws1.column_dimensions[get_column_letter(col)].width = 35

    # col size   Follow Up (1 - 24) + output
    for col in range(43, 67):
        ws1.column_dimensions[get_column_letter(col)].width = 35
    
    ws1.column_dimensions[get_column_letter(67)].width = 30                                                                          

    # row
    for row in range(3,100):
        ws1.row_dimensions[row].height = 120

    ws1.row_dimensions[1].height = 35
    ws1.row_dimensions[2].height = 30

    # Ini buat styling
    for row in range(1,len(all_pasien)+3):
        for col in range(1,68):
            ws1["{}{}".format(get_column_letter(col), row)].border = Border(left=Side(border_style="thin",
                            color='00000000'),
                    right=Side(border_style="thin",
                                color='00000000'),
                    top=Side(border_style="thin",
                            color='00000000'),
                    bottom=Side(border_style="thin",
                                color='00000000'),
                    )

            ws1["{}{}".format(get_column_letter(col), row)].alignment = Alignment(horizontal="center", vertical="center")


            if(ws1.cell(row, 67).value != None) :
                if( (ws1.cell(row, 67).value).lower() == "sembuh"):
                    ws1["{}{}".format(get_column_letter(col), row)].fill = PatternFill("solid", fgColor="c5e0b3")
                
                if( "masih berobat" in (ws1.cell(row, 67).value).lower() ):
                    ws1["{}{}".format(get_column_letter(col), row)].fill = PatternFill("solid", fgColor="ffebf2")

                if( "meninggal" in (ws1.cell(row, 67).value).lower()):
                    ws1["{}{}".format(get_column_letter(col), row)].fill = PatternFill("solid", fgColor="ff0000")
                
            if(col == 7):
                ws1["{}{}".format(get_column_letter(col), row)].fill = PatternFill("solid", fgColor="fff600")

            if(row == 1 or row == 2):
                ws1["{}{}".format(get_column_letter(col), row)].fill = PatternFill("solid", fgColor="00b0f0")

    for row in ws1.iter_rows():
        for cell in row:      
            alignment = copy.copy(cell.alignment)
            alignment.wrapText=True
            cell.alignment = alignment
            
    # Merging
    # A-M
    for i in range(0, 13):
        ws1.merge_cells(start_row=1, end_row=2, start_column=i+1, end_column=i+1)

    # Anamnesis N-U
    ws1.merge_cells(start_row=1, end_row=1, start_column=14, end_column=21)
    # Pemeriksaan Fisik
    ws1.merge_cells(start_row=1, end_row=1, start_column=22, end_column=27)
    # Penunjang
    ws1.merge_cells(start_row=1, end_row=1, start_column=28, end_column=36)

    # AK
    ws1.merge_cells(start_row=1, end_row=2, start_column=37, end_column=37)
    # Sumber kontak dewasa
    ws1.merge_cells(start_row=1, end_row=1, start_column=38, end_column=41)
    # AP
    ws1.merge_cells(start_row=1, end_row=2, start_column=42, end_column=42)
    # Follow Up
    ws1.merge_cells(start_row=1, end_row=1, start_column=43, end_column=66)
    
    # Buat Freeze Column
    ws1.freeze_panes = "J3"

    return wb


    
@login_required(login_url='pasien:login')
def download(_):
    response = HttpResponse(save_virtual_workbook(
        export_to_excel()), content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename="export_pasien_{}.xlsx"'.format(datetime.datetime.now())
    return response