from django import forms
from .models import Pasien

class CheckboxSelectMultipleString(forms.CheckboxSelectMultiple):
    def __init__(self, choices=(), separator=", ", attrs=None):
        super().__init__(attrs=attrs, choices=choices)
        self.separator = separator

    def format_value(self, value):
        return self.separator.join(super().format_value(value))

    def value_from_datadict(self, data, files, name):
        return self.separator.join(super().value_from_datadict(
            data, files, name))

class DateInput(forms.DateInput):
    input_type = 'date'

class PasienForm(forms.ModelForm):
    class Meta:

        model = Pasien
        exclude = ['no']
        labels = {

            'no' : 'No',

            ### Data Pasien ###

            'nama' : 'Nama',
            'no_medrek' : 'NO MEDREK',
            'no_hp' : 'No HP',
            'tanggal_lahir' : 'Tgl Lahir',
            'jenis_kelamin' : 'Jenis Kelamin',
            'tahu_kasus' : 'Tahun Kasus',
            'alamat' : 'Alamat',

            'tanggal_mrs' : 'Tanggal MRS',

            'tanggal_mulai_oat' : 'Tanggal mulai OAT',
            'jumlah_pengobatan_tbc_sebelumnya' : 'Jumlah pengobatan TBC sebelumnya',
            'jenis_resistensi' : 'Jenis Resistensi',
            'jenis_tbc' : 'Jenis TBC',

            ### Anamnesis ###

            'batuk' : 'Batuk',
            'dahak' : 'Dahak',
            'demam' : 'Demam',
            'sesak' : 'Sesak',
            'keringat_malam' : 'Keringat Malam',
            'penurunan_bb' : 'Penurunan BB',
            'hambatan_pertumbuhan' : 'Hambatan Pertumbuhan',
            'penurunan_nafsu_makan' : 'Penurunan Nafsu Makan',

            ### Pemeriksaan Fisik ###
            
            'bb' : 'BB',
            'tb' : 'TB',
            'rr' : 'RR',
            'suhu' : 'Suhu',
            'kgb' : 'KGB',
            'pulmo' : 'Pulmo',

            ### Penunjang ###

            'bta1' : 'BTA 1',
            'bta2' : 'BTA 2',
            'bta3' : 'BTA 3',
            'gene_xpert' : 'Gene Xpert',
            'kultur_mtb' : 'Kultur MTB',
            'drug_sensitivity_test_oat' : 'Drug Sensitivity Test OAT',
            'foto_toraks' : 'Foto Toraks',
            'tes_ppd' : 'Tes PPD',
            'lpa' : 'LPA',

            ### Terapi ###

            'terapi' : 'Terapi',

            
            ### Sumber kontak dewasa ###

            'nama_skd' : 'Nama sumber',
            'hubungan' : 'Hubungan',
            'tanggal_lahir_skd' : 'Tgl lahir',
            'jenis_kasus_tb' : 'Jenis kasus TB',

            ### Tempat ambil obat ###

            'tempat_ambil_obat' : 'Tempat ambil obat',
            
            ### Follow up ###

            'follow_up_1' : '1',
            'follow_up_2' : '2',
            'follow_up_3' : '3',
            'follow_up_4' : '4',
            'follow_up_5' : '5',
            'follow_up_6' : '6',
            'follow_up_7' : '7',
            'follow_up_8' : '8',
            'follow_up_9' : '9',
            'follow_up_10' : '10',
            'follow_up_11' : '11',
            'follow_up_12' : '12',
            'follow_up_13' : '13',
            'follow_up_14' : '14',
            'follow_up_15' : '15',
            'follow_up_16' : '16',
            'follow_up_17' : '17',
            'follow_up_18' : '18',
            'follow_up_19' : '19',
            'follow_up_20' : '20',
            'follow_up_21' : '21',
            'follow_up_22' : '22',
            'follow_up_23' : '23',
            'follow_up_24' : '24',

            ### Output ###

            'output' : 'Output',

        }
        widgets = {


            'nama': forms.TextInput(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'no_medrek': forms.TextInput(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'no_hp': forms.TextInput(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'tanggal_lahir': DateInput(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'jenis_kelamin': forms.Select(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'alamat': forms.Textarea(
                attrs={
                    'class': 'form-control form-section'
                }
            ),

            'tahun_kasus': forms.NumberInput(
                attrs={
                    'class': 'form-control form-section'
                }
            ),

            'tanggal_mrs': DateInput(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            
            'tanggal_mulai_oat': DateInput(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'jumlah_pengobatan_tbc_sebelumnya': forms.TextInput(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'jenis_resistensi': forms.TextInput(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'jenis_tbc': forms.TextInput(
                attrs={
                    'class': 'form-control form-section'
                }
            ),

            # Anamnesis
            
            'batuk': forms.Select(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'dahak': forms.Select(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'demam': forms.Select(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'sesak': forms.Select(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'keringat_malam': forms.Select(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'penurunan_bb': forms.Select(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'hambatan_pertumbuhan': forms.Select(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'penurunan_nafsu_makan': forms.Select(
                attrs={
                    'class': 'form-control form-section'
                }
            ),

            # pemeriksaan_fisik    
            
            'bb': forms.NumberInput(
                attrs={
                    'class': 'form-control form-section',
                    'step': '0.1'
                }
            ),
            'tb': forms.NumberInput(
                attrs={
                    'class': 'form-control form-section',
                    'step': '0.1'
                }
            ),
            'rr': forms.NumberInput(
                attrs={
                    'class': 'form-control form-section',
                    'step': '0.1'
                }
            ),
            'suhu': forms.NumberInput(
                attrs={
                    'class': 'form-control form-section',
                    'step': '0.1'
                }
            ),
            'kgb': forms.TextInput(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'pulmo': forms.TextInput(
                attrs={
                    'class': 'form-control form-section'
                }
            ),

            # penunjang
            'bta1': forms.Select(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'bta2': forms.Select(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'bta3': forms.Select(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'gene_xpert': forms.TextInput(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'kultur_mtb': forms.Select(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'drug_sensitivity_test_oat': forms.TextInput(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'foto_toraks': forms.TextInput(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'tes_ppd': forms.TextInput(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'lpa': forms.TextInput(
                attrs={
                    'class': 'form-control form-section'
                }
            ),


            'terapi': forms.TextInput(
                attrs={
                    'class': 'form-control form-section'
                }
            ),

            # sumber_kontak_dewasa
            'nama_skd': forms.TextInput(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'hubungan': forms.TextInput(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'tanggal_lahir_skd': DateInput(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'jenis_kasus_tb': forms.TextInput(
                attrs={
                    'class': 'form-control form-section'
                }
            ),

            #
            'tempat_ambil_obat': forms.TextInput(
                attrs={
                    'class': 'form-control form-section'
                }
            ),

            # follow_up
            'follow_up_1': forms.Textarea(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'follow_up_2': forms.Textarea(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'follow_up_3': forms.Textarea(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'follow_up_4': forms.Textarea(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'follow_up_5': forms.Textarea(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'follow_up_6': forms.Textarea(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'follow_up_7': forms.Textarea(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'follow_up_8': forms.Textarea(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'follow_up_9': forms.Textarea(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'follow_up_10': forms.Textarea(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'follow_up_11': forms.Textarea(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'follow_up_12': forms.Textarea(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'follow_up_13': forms.Textarea(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'follow_up_14': forms.Textarea(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'follow_up_15': forms.Textarea(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'follow_up_16': forms.Textarea(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'follow_up_17': forms.Textarea(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'follow_up_18': forms.Textarea(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'follow_up_19': forms.Textarea(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'follow_up_20': forms.Textarea(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'follow_up_21': forms.Textarea(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'follow_up_22': forms.Textarea(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'follow_up_23': forms.Textarea(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
            'follow_up_24': forms.Textarea(
                attrs={
                    'class': 'form-control form-section'
                }
            ),

            #
            'output': forms.TextInput(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
        
        }


class ExcelUploadForm(forms.Form):
    excel_file = forms.FileField()
