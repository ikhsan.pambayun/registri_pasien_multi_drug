from django.db import models

# Create your models here.


class Pasien(models.Model):

    ADA_TIDAK_CHOICES = (("ada", "ada"), ("tidak ada", "tidak ada"))

    JENIS_KELAMIN_CHOICES = (("Laki-laki", "Laki-laki"),
                             ("Perempuan", "Perempuan"))

    POSITIF_CHOICES = (("Positif", "Positif"),
                        ("Negatif", "Negatif"))        



    ### DATA PASIEN ###
    no = models.AutoField(primary_key=True)

    nama = models.CharField(max_length=255, blank=True, null=True)

    no_medrek = models.CharField(max_length=255, blank=True, null=True)

    no_hp = models.CharField(max_length=255, blank=True, null=True)

    tanggal_lahir = models.DateField(blank=True, null=True)

    jenis_kelamin = models.CharField(max_length=255,
                                    blank=True,
                                    null=True,
                                    choices=JENIS_KELAMIN_CHOICES)

    tahun_kasus = models.PositiveIntegerField(blank=True, null=True)

    alamat = models.TextField(max_length=255, blank=True, null=True)
    
    tanggal_mrs = models.DateField(blank=True, null=True)

    tanggal_mulai_oat = models.DateField(blank=True, null=True)

    jumlah_pengobatan_tbc_sebelumnya = models.CharField(max_length=255, blank=True, null=True)

    jenis_resistensi = models.CharField(max_length=255, blank=True, null=True)

    jenis_tbc = models.CharField(max_length=255, blank=True, null=True)


    ### Anamnesis ###

    batuk = models.CharField(max_length=255, blank=True, null=True, choices=ADA_TIDAK_CHOICES)

    dahak = models.CharField(max_length=255, blank=True, null=True, choices=ADA_TIDAK_CHOICES)

    demam = models.CharField(max_length=255, blank=True, null=True, choices=ADA_TIDAK_CHOICES)

    sesak = models.CharField(max_length=255, blank=True, null=True, choices=ADA_TIDAK_CHOICES)

    keringat_malam = models.CharField(max_length=255, blank=True, null=True, choices=ADA_TIDAK_CHOICES)

    penurunan_bb = models.CharField(max_length=255, blank=True, null=True, choices=ADA_TIDAK_CHOICES)

    hambatan_pertumbuhan = models.CharField(max_length=255, blank=True, null=True, choices=ADA_TIDAK_CHOICES)

    penurunan_nafsu_makan = models.CharField(max_length=255, blank=True, null=True, choices=ADA_TIDAK_CHOICES)

    ### Pemeriksaan Fisisk ###

    bb = models.CharField(max_length=255, blank=True,null=True)

    tb = models.CharField(max_length=255, blank=True,null=True)

    rr = models.CharField(max_length=255, blank=True,null=True)

    suhu = models.CharField(max_length=255, blank=True, null=True)

    kgb = models.CharField(max_length=255, blank=True, null=True)

    pulmo = models.CharField(max_length=255, blank=True, null=True)

    ### Penunjang ### 

    bta1 = models.CharField(max_length=255, blank=True, null=True, choices=POSITIF_CHOICES)

    bta2 = models.CharField(max_length=255, blank=True, null=True, choices=POSITIF_CHOICES)

    bta3 = models.CharField(max_length=255, blank=True, null=True, choices=POSITIF_CHOICES)

    gene_xpert = models.CharField(max_length=255, blank=True, null=True)

    kultur_mtb = models.CharField(max_length=255, blank=True, null=True)

    drug_sensitivity_test_oat = models.CharField(max_length=255, blank=True, null=True)

    foto_toraks = models.CharField(max_length=255, blank=True, null=True)

    tes_ppd = models.CharField(max_length=255, blank=True, null=True)

    lpa = models.CharField(max_length=255, blank=True, null=True)

    ### Terapi ###

    terapi = models.CharField(max_length=255, blank=True, null=True) 

    ### Sumber Kontak Dewasa ### 

    nama_skd = models.CharField(max_length=255, blank=True, null=True)
    
    hubungan = models.CharField(max_length=255, blank=True, null=True)

    tanggal_lahir_skd = models.DateField(blank=True, null=True)

    jenis_kasus_tb = models.CharField(max_length=255, blank=True, null=True)

    ### Tempat ambil obat ### 

    tempat_ambil_obat = models.CharField(max_length=255, blank=True, null=True)

    ### Follow Up ###

    follow_up_1 = models.TextField(max_length=255, blank=True, null=True)

    follow_up_2 = models.TextField(max_length=255, blank=True, null=True)

    follow_up_3 = models.TextField(max_length=255, blank=True, null=True)

    follow_up_4 = models.TextField(max_length=255, blank=True, null=True)

    follow_up_5 = models.TextField(max_length=255, blank=True, null=True)

    follow_up_6 = models.TextField(max_length=255, blank=True, null=True)

    follow_up_7 = models.TextField(max_length=255, blank=True, null=True)

    follow_up_8 = models.TextField(max_length=255, blank=True, null=True)

    follow_up_9 = models.TextField(max_length=255, blank=True, null=True)

    follow_up_10 = models.TextField(max_length=255, blank=True, null=True)

    follow_up_11 = models.TextField(max_length=255, blank=True, null=True)

    follow_up_12 = models.TextField(max_length=255, blank=True, null=True)

    follow_up_13 = models.TextField(max_length=255, blank=True, null=True)

    follow_up_14 = models.TextField(max_length=255, blank=True, null=True)
    
    follow_up_15 = models.TextField(max_length=255, blank=True, null=True)

    follow_up_16 = models.TextField(max_length=255, blank=True, null=True)

    follow_up_17 = models.TextField(max_length=255, blank=True, null=True)

    follow_up_18 = models.TextField(max_length=255, blank=True, null=True)

    follow_up_19 = models.TextField(max_length=255, blank=True, null=True)

    follow_up_20 = models.TextField(max_length=255, blank=True, null=True)

    follow_up_21 = models.TextField(max_length=255, blank=True, null=True)

    follow_up_22 = models.TextField(max_length=255, blank=True, null=True)

    follow_up_23 = models.TextField(max_length=255, blank=True, null=True)

    follow_up_24 = models.TextField(max_length=255, blank=True, null=True)

    #### Output ###

    output = models.CharField(max_length=255, blank=True, null=True)


    def __str__(self):
        return self.nama

    def get_fields(self):
        return [(field.name, field.value_to_string(self)) for field in Pasien._meta.fields]

    def get_header():
        return [(field.name) for field in Pasien._meta.fields]

    def get_all_value(self):
        return [field.value_from_object(self) for field in Pasien._meta.fields]

    def dict_value(self): 
        return {field.name : field.value_from_object(self) for field in Pasien._meta.fields}