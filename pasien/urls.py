from django.urls import path
from . import views

app_name = 'pasien'

urlpatterns = [

    path('', views.list_view, name='index'),
    path('forms/', views.forms, name='form_pasien'),
    path('delete/<int:nomor>', views.delete_pasien, name='delete_pasien'),
    path('edit/<int:nomor>', views.edit_pasien, name='edit_pasien'),
    # path('import/', views.import_file, name='import'),
    path('export/', views.download, name='export'),
    path('login/', views.loginAdmin, name='login'),
    path('logout/', views.logoutAdmin, name='logout'),
]
